package com.fariasmaxi.kafka.controllers

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import java.time.Instant

@RestController
class SendMessage(@Autowired private val kafkaTemplate: KafkaTemplate<String, Message>,
                  private val topicName: String = "spring") {

    @PostMapping("/messages")
    operator fun invoke(message: String): ResponseEntity<Message> {
        val response = Message(message, Instant.now().epochSecond)
        kafkaTemplate.send(topicName, response)
        return ResponseEntity.ok(response)
    }

}

data class Message(@JsonProperty("value") val value: String,
                   @JsonProperty("timestamp") val timestamp: Long)
