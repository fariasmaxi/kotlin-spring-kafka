package com.fariasmaxi.kafka

import com.fariasmaxi.kafka.controllers.Message
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.kafka.annotation.KafkaListener

@SpringBootApplication
class Application{

    @KafkaListener(topics = ["spring"], groupId = "spring", containerFactory = "messageKafkaListenerContainerFactory")
    fun messageListener(message: Message) {
        println("Received Message in group 'spring': $message")
    }
}

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
